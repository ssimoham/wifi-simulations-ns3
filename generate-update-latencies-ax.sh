declare -a trafficDirections=("upstream" "downstream" )

for probingDirection in ${trafficDirections[@]}; do
    for i in $(eval echo {$1..$2})
    do
        path="wifi/update/latency/probing-${probingDirection}/ax/graphs"
        mkdir "${path}/txt" "${path}/parsed" "${path}/csv"
        num=$(( $i - 1 ))
        
        ./waf --run "scratch/wifi-update-latency-ax.cc --nWifi=$i --probingDirection=$probingDirection" 2> "${path}/txt/$num.txt"
        cat "${path}/txt/$num.txt" | grep -e 'At time +[0-9]*s client sent 1024 bytes' -e 'server received 1024 bytes from' > "${path}/parsed/$num.txt"
        python3 get_latencies.py "${path}/parsed/$num.txt" "${path}/csv/$num.csv"
    done
done