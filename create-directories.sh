mkdir -p ../wifi/overload/throughput/downstream/ac/traces
mkdir -p ../wifi/overload/throughput/downstream/ac/graphs
mkdir -p ../wifi/overload/throughput/downstream/ax/traces
mkdir -p ../wifi/overload/throughput/downstream/ax/graphs
mkdir -p ../wifi/overload/throughput/upstream/ac/traces
mkdir -p ../wifi/overload/throughput/upstream/ac/graphs
mkdir -p ../wifi/overload/throughput/upstream/ax/traces
mkdir -p ../wifi/overload/throughput/upstream/ax/graphs

mkdir -p ../wifi/overload/latency/upstream/probing-upstream/ac/traces
mkdir -p ../wifi/overload/latency/upstream/probing-upstream/ac/graphs
mkdir -p ../wifi/overload/latency/upstream/probing-upstream/ax/traces
mkdir -p ../wifi/overload/latency/upstream/probing-upstream/ax/graphs

mkdir -p ../wifi/telemetry/latency/probing-upstream/ac/traces
mkdir -p ../wifi/telemetry/latency/probing-upstream/ac/graphs
mkdir -p ../wifi/telemetry/latency/probing-upstream/ax/traces
mkdir -p ../wifi/telemetry/latency/probing-upstream/ax/graphs

mkdir -p ../wifi/notifications/latency/probing-downstream/ac/traces
mkdir -p ../wifi/notifications/latency/probing-downstream/ac/graphs
mkdir -p ../wifi/notifications/latency/probing-downstream/ax/traces
mkdir -p ../wifi/notifications/latency/probing-downstream/ax/graphs

mkdir -p ../wifi/surveillance/latency/probing-upstream/ac/traces
mkdir -p ../wifi/surveillance/latency/probing-upstream/ac/graphs
mkdir -p ../wifi/surveillance/latency/probing-upstream/ax/traces
mkdir -p ../wifi/surveillance/latency/probing-upstream/ax/graphs