declare -a trafficDirections=("upstream")

for probingDirection in ${trafficDirections[@]}; do
    for i in $(eval echo {$1..$2})
    do
        for k in {1..5};
        do
            seed=$(($k))
            path="../wifi/surveillance/latency/probing-${probingDirection}/ax/graphs"
            mkdir "${path}/txt" "${path}/parsed" "${path}/csv"
            num=$(( $i - 1 ))
            echo "NSta=$num" > "${path}/csv/$num-$seed.csv"
            
            ./waf --run "scratch/wifi-surveillance-latency-ax.cc --seed=$seed --nWifi=$i --probingDirection=$probingDirection" 2> "${path}/txt/$num-$seed.txt"
            cat "${path}/txt/$num-$seed.txt" | grep -e 'At time [0-9]* client sent 1023 bytes' -e 'server received 1023 bytes from' > "${path}/parsed/$num-$seed.txt"
            python3 get_latencies.py "${path}/parsed/$num-$seed.txt" "${path}/csv-/$num.csv"
        done
    done
done
