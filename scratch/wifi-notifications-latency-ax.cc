#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/ssid.h"
#include "ns3/wifi-net-device.h"
#include "ns3/wifi-mac-header.h"
#include "ns3/wifi-mac.h"
#include "ns3/random-variable-stream.h"
#include "ns3/core-module.h"
#include <iostream>
#include <fstream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("wifi-notifications-latency-ax");

int main (int argc, char *argv[]) {
  SeedManager::SetSeed (3);  // Changes seed from default of 1 to 3
  SeedManager::SetRun (1);  // Changes run number from default of 1 to 7
  uint32_t nWifi = 3;// 1 probing station + (nWifi-1) overloading stations
  double xDistance = 1;// Meters between AP and other stations
  bool agregation = false; // Allow or not the packet agregation
  std::string probingDirection = "downstream";

  double simulationTime = 100; //Seconds

  CommandLine cmd;
  cmd.AddValue("nWifi", "Number of all stations", nWifi);
  cmd.AddValue("probingDirection", "Probing Direction", probingDirection);
  cmd.Parse(argc, argv);

  std::cout << "Number of overloading stations : " << nWifi-1 << std::endl;
 
  Time::SetResolution (Time::PS);

  LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
  LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);

  NodeContainer wifiStaNodes, wifiApNode, wifiProbingNode;
  wifiStaNodes.Create(2);
  wifiApNode = wifiStaNodes.Get(0);
  wifiProbingNode = wifiStaNodes.Get(1);

  NodeContainer wifiCommNodes;//Communication nodes that overload the network
  wifiCommNodes.Create (nWifi-1);

  YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
  YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  phy.SetChannel (channel.Create ());

  WifiHelper wifi;
  WifiMacHelper mac;

  int mcs = 9; // -1 indicates an unset value
  int channelWidth = 80; // BW Channel Width
  int gi = 800; // Guard Interval
  double frequency = 5;
  uint32_t payloadSize = 2048; //1500 byte IP packet
  uint32_t payloadSizeEcho = 1024; //Packet size for Echo UDP App

  if (frequency == 6) {
    wifi.SetStandard (WIFI_STANDARD_80211ax_6GHZ);
    Config::SetDefault ("ns3::LogDistancePropagationLossModel::ReferenceLoss", DoubleValue (48));
  }
  else if (frequency == 5) {
    wifi.SetStandard (WIFI_STANDARD_80211ax_5GHZ);
  }
  else if (frequency == 2.4) {
    wifi.SetStandard (WIFI_STANDARD_80211ax_2_4GHZ);
    Config::SetDefault ("ns3::LogDistancePropagationLossModel::ReferenceLoss", DoubleValue (40));
  }
  else {
    std::cout << "Wrong frequency value!" << std::endl;
    return 0;
  }

  std::ostringstream oss;
  oss << "HeMcs" << mcs;
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager","DataMode", StringValue (oss.str ()),
                                "ControlMode", StringValue (oss.str ()));

  Ssid ssid = Ssid ("ns-3-ssid");

  // Installing phy & mac layers on the overloading stations
  NetDeviceContainer commDevices;
  mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssid));
  commDevices = wifi.Install (phy, mac, wifiCommNodes);
  mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssid));

  // Installing phy & mac layers on the probing station
  NetDeviceContainer probingDevice;
  probingDevice = wifi.Install(phy, mac, wifiProbingNode);

  // Installing phy & mac layers on the AP
  mac.SetType("ns3::ApWifiMac",
              "EnableBeaconJitter", BooleanValue (false),
              "Ssid", SsidValue(ssid));
  NetDeviceContainer apDevice;
  apDevice = wifi.Install(phy, mac, wifiApNode);
  
  // Set channel width, guard interval and MPDU buffer size
  Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/ChannelWidth", UintegerValue (channelWidth));
  Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HeConfiguration/GuardInterval", TimeValue (NanoSeconds (gi)));
  Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HeConfiguration/MpduBufferSize", UintegerValue (64));

  // Setting stations' positions
  MobilityHelper mobility;
  mobility.SetPositionAllocator("ns3::GridPositionAllocator",
    "MinX", DoubleValue(0.0),
    "MinY", DoubleValue(0.0),
    "DeltaX", DoubleValue(xDistance),
    "DeltaY", DoubleValue(0.0),
    "GridWidth", UintegerValue(3),
    "LayoutType", StringValue("RowFirst"));

  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(wifiStaNodes);

  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();

  for (uint32_t i = 0; i < nWifi; i++) {
    positionAlloc->Add (Vector (xDistance, 0.0, 0.0));
  }
  mobility.SetPositionAllocator (positionAlloc);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (wifiCommNodes);

  // Internet stack
  InternetStackHelper stack;
  stack.Install(wifiStaNodes);
  stack.Install (wifiCommNodes);

  // Setting IP addresses
  Ipv4AddressHelper address;
  Ipv4InterfaceContainer wifiProbingInterface, wifiApInterface, commInterfaces;
  address.SetBase("10.1.1.0", "255.255.255.0");
  wifiApInterface = address.Assign(apDevice);// 10.0.0.1 for AP
  wifiProbingInterface = address.Assign(probingDevice);
  commInterfaces = address.Assign (commDevices);// 10.0.0.2 for Probing station

  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

  // UDP Echo Server application to be installed in the AP
  int echoPort = 9;
  UdpEchoServerHelper echoServer(echoPort); // Port # 9

  if (probingDirection == "upstream") {
    ApplicationContainer serverApps = echoServer.Install(wifiApNode);
    serverApps.Start(Seconds(0.0));
    serverApps.Stop(Seconds(simulationTime+1));
    
    //wifiApInterface.GetAddress(0).Print(std::cout);
    // UDP Echo Client application to be installed in the probing station
    UdpEchoClientHelper echoClient1(wifiApInterface.GetAddress(0), echoPort);
    
    echoClient1.SetAttribute("MaxPackets", UintegerValue(100));
    echoClient1.SetAttribute("Interval", TimeValue(Seconds(2)));
    echoClient1.SetAttribute("PacketSize", UintegerValue(payloadSizeEcho));

    ApplicationContainer clientApps1 = echoClient1.Install(wifiProbingNode);
    clientApps1.Start(Seconds(1.0));
    clientApps1.Stop(Seconds(simulationTime+1));
  }
  else {
    ApplicationContainer serverApps = echoServer.Install(wifiProbingNode);
    serverApps.Start(Seconds(0.0));
    serverApps.Stop(Seconds(simulationTime+1));

    // UDP Echo Client application to be installed in the probing station
    UdpEchoClientHelper echoClient1(wifiProbingInterface.GetAddress(0), echoPort);
    
    echoClient1.SetAttribute("MaxPackets", UintegerValue(100));
    echoClient1.SetAttribute("Interval", TimeValue(Seconds(2)));
    echoClient1.SetAttribute("PacketSize", UintegerValue(payloadSizeEcho));

    ApplicationContainer clientApps1 = echoClient1.Install(wifiApNode);
    clientApps1.Start(Seconds(1.0));
    clientApps1.Stop(Seconds(simulationTime+1));
  }

  if (agregation == false) {
    // Disable A-MPDU & A-MSDU in AP
    Ptr<NetDevice> dev = wifiStaNodes.Get(0)-> GetDevice(0);
    Ptr<WifiNetDevice> wifi_dev = DynamicCast<WifiNetDevice> (dev);
    wifi_dev->GetMac ()->SetAttribute ("BE_MaxAmpduSize", UintegerValue (0));
    wifi_dev->GetMac ()->SetAttribute ("BE_MaxAmsduSize", UintegerValue (0));

    // Disable A-MPDU & A-MSDU in Probing Node
    dev = wifiStaNodes.Get(1)-> GetDevice(0);
    wifi_dev = DynamicCast<WifiNetDevice> (dev);
    wifi_dev->GetMac ()->SetAttribute ("BE_MaxAmpduSize", UintegerValue (0));
    wifi_dev->GetMac ()->SetAttribute ("BE_MaxAmsduSize", UintegerValue (0));
  }

  /* Setting applications */
  ApplicationContainer sourceApplications, sinkApplications;
  uint32_t portNumber = 11;
  double min = 0.0;
  double max = 0.5;

  for (uint32_t index = 0; index < nWifi-1; ++index) {
    if (agregation == false) {
      // Disable A-MPDU & A-MSDU in each station
      Ptr<NetDevice> dev = wifiCommNodes.Get (index)->GetDevice (0);
      Ptr<WifiNetDevice> wifi_dev = DynamicCast<WifiNetDevice> (dev);
      wifi_dev->GetMac ()->SetAttribute ("BE_MaxAmpduSize", UintegerValue (0));
      wifi_dev->GetMac ()->SetAttribute ("BE_MaxAmsduSize", UintegerValue (0));
    }

    auto ipv4 = wifiApNode.Get (0)->GetObject<Ipv4> ();
    const auto address = ipv4->GetAddress (1, 0).GetLocal ();
    InetSocketAddress sinkSocket (address, portNumber);
    
    // UDP Client application to be installed in the stations
    UdpClientHelper echoClient2(address, portNumber);
  
    echoClient2.SetAttribute("MaxPackets", UintegerValue(1000000));
    echoClient2.SetAttribute("Interval", TimeValue(Seconds(1)));
    echoClient2.SetAttribute("PacketSize", UintegerValue(payloadSize));

    Ptr<UniformRandomVariable> x = CreateObject<UniformRandomVariable> ();
    x->SetAttribute ("Min", DoubleValue (min));
    x->SetAttribute ("Max", DoubleValue (max));

    double value = 1 + x->GetValue ();
    //std::cout << value << std::endl;

    ApplicationContainer sourceApplications1 = echoClient2.Install (wifiCommNodes.Get(index));
    sourceApplications1.Start(Seconds(value));
    sourceApplications1.Stop(Seconds(simulationTime+value));
    //sourceApplications.Add (echoClient2.Install (wifiCommNodes.Get(index)));
    
    UdpServerHelper server (portNumber++);
    sinkApplications.Add(server.Install (wifiApNode));
  }

  // Start the UDP Client & Server applications
  sinkApplications.Start (Seconds (1.0));
  sinkApplications.Stop (Seconds (simulationTime + 1));
  //sourceApplications.Start (Seconds (1.0));
  //sourceApplications.Stop (Seconds (simulationTime + 1));

  Ptr<MobilityModel> model1 = wifiStaNodes.Get(0)->GetObject<MobilityModel>();
  Ptr<MobilityModel> model2 = wifiCommNodes.Get(0)->GetObject<MobilityModel>();

  double distance = model1->GetDistanceFrom (model2);
  printf("\nDistance : %.2lf m\n",distance);

	AsciiTraceHelper ascii;
	
  phy.SetPcapDataLinkType (WifiPhyHelper::DLT_IEEE802_11_RADIO);
  std::string s = "../wifi/notifications/latency/probing-" + probingDirection + "/ax/traces/" + std::to_string(nWifi-1);
  phy.EnableAsciiAll (ascii.CreateFileStream(s+".tr"));
  phy.EnablePcap (s+".pcap", apDevice.Get(0), false, true);

  Simulator::Stop (Seconds (simulationTime + 1));
  Simulator::Run ();

  Simulator::Destroy ();
  return 0;
}
