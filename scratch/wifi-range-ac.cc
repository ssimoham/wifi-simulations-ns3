#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/ssid.h"
#include <iostream>
#include <fstream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("ThirdScriptExample");



int main (int argc, char *argv[])
{
  SeedManager::SetSeed (3);  // Changes seed from default of 1 to 3
  SeedManager::SetRun (5);  // Changes run number from default of 1 to 7
  double xDistance = 1;
  CommandLine cmd;
  cmd.AddValue("xDistance", "Distance between two nodes along x-axis", xDistance);
  cmd.Parse(argc, argv);
 
  Time::SetResolution (Time::NS);

  LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
  LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);

  NodeContainer wifiStaNodes, wifiApNode;
  wifiStaNodes.Create(2);
  wifiApNode = wifiStaNodes.Get(0);


  YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
  YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  phy.SetChannel (channel.Create ());

  WifiHelper wifi;
  WifiMacHelper mac;

  int mcs = 9; // -1 indicates an unset value
  int channelWidth = 80; // BW Channel Width
  int sgi = 1; // Indicates whether Short Guard Interval is enabled or not (SGI==1 <==> GI=400ns)
  uint32_t payloadSize = 1024; //Packet size for Echo UDP App
  
  wifi.SetStandard (WIFI_PHY_STANDARD_80211ac);

  std::ostringstream oss;
  oss << "VhtMcs" << mcs;
  
  // Set MCS
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager","DataMode", StringValue (oss.str ()),
                                "ControlMode", StringValue (oss.str ()));

  Ssid ssid = Ssid ("ns-3-ssid");
  mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssid));

  NetDeviceContainer staDevices;
  staDevices = wifi.Install(phy, mac, wifiStaNodes.Get(1));

  mac.SetType("ns3::ApWifiMac",
              "EnableBeaconJitter", BooleanValue (false),
              "Ssid", SsidValue(ssid));
  NetDeviceContainer apDevice;
  apDevice = wifi.Install(phy, mac, wifiApNode);

  // Set channel width
  Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/ChannelWidth", UintegerValue (channelWidth));
  
  // Set guard interval
  Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HtConfiguration/ShortGuardIntervalSupported", BooleanValue (sgi));

  MobilityHelper mobility;
  mobility.SetPositionAllocator("ns3::GridPositionAllocator",
    "MinX", DoubleValue(0.0),
    "MinY", DoubleValue(0.0),
    "DeltaX", DoubleValue(xDistance),
    "DeltaY", DoubleValue(0.0),
    "GridWidth", UintegerValue(3),
    "LayoutType", StringValue("RowFirst"));

  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(wifiStaNodes);
  InternetStackHelper stack;
  stack.Install(wifiStaNodes);

  Ipv4AddressHelper address;
  Ipv4InterfaceContainer wifiInterfaces, wifiApInterface;
  address.SetBase("10.1.1.0", "255.255.255.0");
  wifiApInterface = address.Assign(apDevice);
  wifiInterfaces = address.Assign(staDevices);

  UdpEchoServerHelper echoServer(9); // Port # 9

  ApplicationContainer serverApps = echoServer.Install(wifiApNode);
  serverApps.Start(Seconds(1.0));
  serverApps.Stop(Seconds(30.0));
  wifiApInterface.GetAddress(0).Print(std::cout);

  UdpEchoClientHelper echoClient(wifiApInterface.GetAddress(0), 9);
   
  echoClient.SetAttribute("MaxPackets", UintegerValue(7));
  echoClient.SetAttribute("Interval", TimeValue(Seconds(3)));
  echoClient.SetAttribute("PacketSize", UintegerValue(payloadSize));

  ApplicationContainer clientApps = echoClient.Install(wifiStaNodes.Get(1));
  wifiInterfaces.GetAddress(0).Print(std::cout);
  clientApps.Start(Seconds(5.0));
  clientApps.Stop(Seconds(30.0));

  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

	Simulator::Stop(Seconds(30.0));

  Ptr<MobilityModel> model1 = wifiStaNodes.Get(0)->GetObject<MobilityModel>();
  Ptr<MobilityModel> model2 = wifiStaNodes.Get(1)->GetObject<MobilityModel>();

  double distance = model1->GetDistanceFrom (model2);

  printf("\nDistance : %.2lf m\n",distance);

	//AsciiTraceHelper ascii;
	//phy.EnableAsciiAll (ascii.CreateFileStream("wifi.tr"));

  phy.SetPcapDataLinkType (WifiPhyHelper::DLT_IEEE802_11_RADIO);
	phy.EnablePcap ("test-range", apDevice.Get(0));

  Simulator::Stop (Seconds (30.0));

  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
