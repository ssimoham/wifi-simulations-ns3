#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/ssid.h"
#include <iostream>
#include <fstream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("ThirdScriptExample");

int main (int argc, char *argv[]) {
  double xDistance = 20;
  CommandLine cmd;
  cmd.AddValue("xDistance", "Distance between two nodes along x-axis", xDistance);
  cmd.Parse(argc, argv);
 
  Time::SetResolution (Time::NS);

  LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
  LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);

  NodeContainer wifiStaNodes, wifiApNode;
  wifiStaNodes.Create(2);
  wifiApNode = wifiStaNodes.Get(0);

  YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
  YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  phy.SetChannel (channel.Create ());

  WifiHelper wifi;
  WifiMacHelper mac;

  int mcs = 5; // -1 indicates an unset value
  int channelWidth = 20; // BW Channel Width
  int gi = 800; // Guard Interval
  double frequency = 5;
  uint32_t payloadSize = 1024; // Packet size

  if (frequency == 6) {
    wifi.SetStandard (WIFI_STANDARD_80211ax_6GHZ);
    Config::SetDefault ("ns3::LogDistancePropagationLossModel::ReferenceLoss", DoubleValue (48));
  }
  else if (frequency == 5) {
    wifi.SetStandard (WIFI_STANDARD_80211ax_5GHZ);
  }
  else if (frequency == 2.4) {
    wifi.SetStandard (WIFI_STANDARD_80211ax_2_4GHZ);
    Config::SetDefault ("ns3::LogDistancePropagationLossModel::ReferenceLoss", DoubleValue (40));
  }
  else {
    std::cout << "Wrong frequency value!" << std::endl;
    return 0;
  }
  
  std::ostringstream oss;
  oss << "HeMcs" << mcs;
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager","DataMode", StringValue (oss.str ()),
                                "ControlMode", StringValue (oss.str ()));

  Ssid ssid = Ssid ("ns-3-ssid");
  mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssid));

  NetDeviceContainer staDevices;
  staDevices = wifi.Install(phy, mac, wifiStaNodes.Get(1));

  mac.SetType("ns3::ApWifiMac",
              "EnableBeaconJitter", BooleanValue (false),
              "Ssid", SsidValue(ssid));
  NetDeviceContainer apDevice;
  apDevice = wifi.Install(phy, mac, wifiApNode);

  // Set channel width, guard interval and MPDU buffer size
  Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/ChannelWidth", UintegerValue (channelWidth));
  Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HeConfiguration/GuardInterval", TimeValue (NanoSeconds (gi)));
  Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HeConfiguration/MpduBufferSize", UintegerValue (64));

  MobilityHelper mobility;
  mobility.SetPositionAllocator("ns3::GridPositionAllocator",
    "MinX", DoubleValue(0.0),
    "MinY", DoubleValue(0.0),
    "DeltaX", DoubleValue(xDistance),
    "DeltaY", DoubleValue(0.0),
    "GridWidth", UintegerValue(3),
    "LayoutType", StringValue("RowFirst"));

  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(wifiStaNodes);
  InternetStackHelper stack;
  stack.Install(wifiStaNodes);

  Ipv4AddressHelper address;
  Ipv4InterfaceContainer wifiInterfaces, wifiApInterface;
  address.SetBase("10.1.1.0", "255.255.255.0");
  wifiApInterface = address.Assign(apDevice);
  wifiInterfaces = address.Assign(staDevices);

  UdpEchoServerHelper echoServer(9); // Port # 9

  ApplicationContainer serverApps = echoServer.Install(wifiApNode);
  serverApps.Start(Seconds(1.0));
  serverApps.Stop(Seconds(10.0));
  wifiApInterface.GetAddress(0).Print(std::cout);

  UdpEchoClientHelper echoClient(wifiApInterface.GetAddress(0), 9);
   
  echoClient.SetAttribute("MaxPackets", UintegerValue(10));
  echoClient.SetAttribute("Interval", TimeValue(Seconds(2)));
  echoClient.SetAttribute("PacketSize", UintegerValue(payloadSize));

  ApplicationContainer clientApps = echoClient.Install(wifiStaNodes.Get(1));
  wifiInterfaces.GetAddress(0).Print(std::cout);
  clientApps.Start(Seconds(3.0));
  clientApps.Stop(Seconds(10.0));

  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

	Simulator::Stop(Seconds(10.0));

  Ptr<MobilityModel> model1 = wifiStaNodes.Get(0)->GetObject<MobilityModel>();
  Ptr<MobilityModel> model2 = wifiStaNodes.Get(1)->GetObject<MobilityModel>();

  double distance = model1->GetDistanceFrom (model2);

  printf("\nDistance : %.2lf m\n",distance);
  
  //AsciiTraceHelper ascii;
 //phy.EnableAsciiAll (ascii.CreateFileStream("wifi.tr"));

  phy.SetPcapDataLinkType (WifiPhyHelper::DLT_IEEE802_11_RADIO);
  phy.EnablePcap ("test-range", wifiApNode);

  Simulator::Stop (Seconds (10.0));

  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
