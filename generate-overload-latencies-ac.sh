declare -a trafficDirections=("upstream" )

for trafficDirection in ${trafficDirections[@]}; do
        for i in $(eval echo {$1..$2})
        do
            path="../wifi/overload/latency/${trafficDirection}/probing-${trafficDirection}/ac/graphs"
            mkdir "${path}/txt" "${path}/parsed" "${path}/csv"
            num=$(( $i - 1 ))
            echo "NSta=$num" > "${path}/csv/$num.csv"

            ./waf --run "scratch/wifi-overload-latency-ac.cc --nWifi=$i --trafficDirection=$trafficDirection --probingDirection=$trafficDirection" 2> "${path}/txt/$num.txt"
            cat "${path}/txt/$num.txt" | grep -e 'At time [0-9]* client sent 1024 bytes' -e 'server received 1024 bytes from' > "${path}/parsed/$num.txt"
            python3 get_latencies.py "${path}/parsed/$num.txt" "${path}/csv/$num.csv"
        done
done